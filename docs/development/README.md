# Development

## Dev tools

Read [install-dev-tools.md](/docs/development/install-dev-tools.md)


## Build

__Backend__

```bash
./mvnw clean install
```

to build a specific service :

```bash
./mvnw clean install -pl service/$MY_SERVICE -am
```


## Run

__Backend__

```bash
export SPRING_PROFILES_ACTIVE=dev
java -jar service/$MY_SERVICE/target/$MY_SERVICE-dev.jar
```


## Run with docker


## Security

Read [security.md](/docs/development/security.md)


## Release process

Read [release_process.md](/docs/development/release_process.md)


## Issues

Read [issues.md](/docs/development/issues.md)

