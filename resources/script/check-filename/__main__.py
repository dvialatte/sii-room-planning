#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    check-filename
    ==============

    This script is used to validate file naming convention.
"""

import os
import re
import sys

def main():
    """
        Main function
    """

    pattern_dir = re.compile(r'^[\w\.\-\/]+$')
    pattern_file = re.compile(r'^[\w\.\-]+$')

    for search_results in os.walk('./'):
        directory = search_results[0]

        if not pattern_dir.match(directory):
            print('Error directory name ( /^[\\w\\.\\-\\/]+$/ ) : ' + directory)
            sys.exit(1)

        for filename in search_results[2]:
            file = os.path.join(directory, filename)

            if pattern_file.match(filename):
                print('[OK] ' + file)
            else:
                print('Error file name ( /^[\\w\\.\\-]+$/ ) : ' + file)
                sys.exit(1)

if __name__ == "__main__":
    main()
